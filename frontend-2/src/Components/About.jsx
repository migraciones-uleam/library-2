import React from 'react'
import './About.css'

function About() {
    return (
        <div className='about-box'>
            <h2 className="about-title">Acerca de nosotros</h2>
            <div className="about-data">
                <div className="about-img">
                    <img src="https://images.unsplash.com/photo-1583468982228-19f19164aee2?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=913&q=80" alt="" />
                </div>
                <div>
                    <p className="about-text">
                        La biblioteca en línea es una poderosa herramienta que permite a los usuarios
                        acceder a una amplia gama de recursos educativos y de entretenimiento desde la
                        comodidad de sus hogares. Esta biblioteca virtual proporciona acceso a una gran
                        cantidad de libros electrónicos, revistas, periódicos, audiolibros y otros
                        materiales digitales. Los usuarios pueden explorar y leer obras literarias
                        clásicas, investigar temas de interés, ampliar su conocimiento en diversas áreas
                        o simplemente disfrutar de una buena lectura.<br/>
                        <br/>
                        Una de las ventajas destacadas de la biblioteca en línea es su disponibilidad las
                        24 horas del día, los 7 días de la semana. Los usuarios no están limitados por
                        los horarios de apertura o cierre de una biblioteca física, lo que les brinda
                        la flexibilidad de acceder al material en cualquier momento que les resulte
                        conveniente. Además, la biblioteca en línea elimina la necesidad de desplazarse
                        físicamente, lo que resulta especialmente beneficioso para aquellos que viven
                        en áreas rurales o tienen dificultades de movilidad.<br/>
                    </p>
                </div>
            </div>
        </div>
    )
}

export default About
